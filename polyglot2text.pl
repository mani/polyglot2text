#!/usr/bin/perl -w
use strict;
use Image::Magick;
use List::MoreUtils qw( minmax );
use Term::ANSIColor;
use Data::Dumper;
use POSIX qw( floor );
my $debug = 1;
$#ARGV != 0 && die "Usage: polyglot2text.pl <video.avi>";
my ($fps, $frm);
open(TMP, "mencoder -v -o /dev/null -endpos 0 -nosound -ovc frameno $ARGV[0] | ") or die "Can't open pipe: $!";
while(<TMP>){
    $fps = $1 if /(\d+)\.\d+ fps/;
    $frm = $1 if /for (\d+) frames/;
}
close(TMP);
my $bn = $ARGV[0];
$bn =~ s|.*/(.*).avi$|$1|;
my ($w, $h, $rate, $suf, $hsq, $fuzz) = (640, 480, 1, "jpg", 6, 256 ** 2 * 0.05);
system ("ffmpeg -v quiet -r " . $fps * $rate . " -i $ARGV[0] -an -ss 10 -r 1 -y -f image2 -s ${w}x$h 'fr${bn}_%04d.$suf' &");# or die "couldn't exec ffmpeg: $!";
sleep 10;
sub debug{
    print STDERR @_ if defined $debug;
}
sub check_rgb{
    for my $c (@_){
	$c = floor($c / 256); # int() not so precise
	if ( $c < 219 || $c > 254 ){
	    debug " out of range: $c";
	    return 1;
	}
    }
    my ( $min, $max ) = minmax @_;
    if ( $max - $min > 22 ){
	debug " out of gray: " . ($max-$min);
	return 1;
    }
    return 0;
}
sub check_slide_point{
    my ($f, $x, $y) = @_;
    my ($flag, $init_skip) = (0, 3);
    my $skips = $init_skip;
    for my $rl ([{x=>$x,     y=>$y},
		 {x=>$x,     y=>$y-$hsq},
		 {x=>$x-$hsq,y=>$y},],
		[{x=>$hsq,   y=>$y},
		 {x=>$hsq,   y=>$y-$hsq},
		 {x=>2*$hsq, y=>$y}]){
	my $skips;
	for my $xy (@$rl){
	    if ( check_rgb( $f->Image::Magick::GetPixel(%$xy, channel=>"RGB", normalize=>0) ) == 0 ){
		debug " ok";
		$skips = 1;
		last;
	    }
	}
	if ( ! defined($skips) ){
	    debug colored(['reverse'], " declined\n");
	    return 1;
	}
    }
    debug("\n");
    return 0;
}
sub find_slide_bound{
    my ($f, $x, $y) = @_;
    my $up = 0;
    while( $y - $up >= $hsq ){
	my $ord = $up + ($y - $up) / 2;
	debug "check $up-$y=$ord";
	check_slide_point($f, $x, $ord) == 1 ? $up = $ord : $y = $ord;
    }
    return $y;
}
sub rm{
    my ($i) = @_;
    unlink $i or die "Could not unlink $i: $!";
    #rename $i, "notslide/$i" or die "Can't rename: $!";
}
my ($prev, $pdif, $white, $pwhite);
my $i;
#for $i (glob("fr${bn}_*")){
for my $I (1 .. 9999){
    $i = sprintf("fr${bn}_%04d.$suf", $I);
    if( ! -f $i ){
	sleep 1;
	print ".";
	last if ! -f $i;
    }
    debug($i);
    my $img = new Image::Magick;
    $img->Read($i);
    if ( check_slide_point($img, $w - 10, $h - 5) != 0 ){ rm($i);next; }
    my $y = find_slide_bound($img, $w - 10, $h - 5 - 2 * $hsq);
    if ( $h - $y <= 35 ){ rm($i);next; }
    if ( $y > $hsq ){
	$img->Draw(fill=>"white", primitive=>"rectangle", points=>"0,0 $w,$y");
    }
    $img->WhiteThreshold(threshold=>"78%", channel=>"RGB");
    if ( ! defined($white) ){
	#$white = new Image::Magick;
	#$white->Read("white.png");
	#$white = $img->Clone();
	#$white->Draw(fill=>"white", primitive=>"rectangle", points=>"0,0 $w,$h");
	$white = Image::Magick->new(size=>"${w}x$h");
	$white->ReadImage('canvas:white');
    }
    my $cwhite = $white->Clone();
    $cwhite->Composite(image=>$img, compose=>"Src"); # direction sensitive
    my $cdif = $cwhite->Compare(image=>$white, metric=>"AE", fuzz=> $fuzz);
    $cdif = $cdif->Get('error');
    if ( $cdif <= 10 ){
	rm($i);next;
    }
    if ( defined($prev) ){
	my $cpdif = $cwhite->Compare(image=>$pwhite, metric=>"AE", fuzz=> $fuzz);
	$cpdif = $cpdif->Get('error');
	debug("dif: $pdif $cpdif $cdif");
	if ( $cpdif < $cdif && ( $pdif < $cdif || abs($pdif / $cdif - 1) < 0.01 ) ){
	    debug colored(['reverse'], " removed");
	    rm($prev);
	}
	undef $pwhite->[0];
	undef $pwhite;
    }
    $img->Write($i);
    debug("\n");
    $pwhite = $cwhite;
    $pdif = $cdif;
    $prev = $i;
    undef $img->[0];
    undef $img;
}
