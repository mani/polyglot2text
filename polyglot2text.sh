#!/bin/sh
#DEBUG=
[ $# != 1 ] && echo "Usage: $0 <video.avi>" && exit 1
TMP=`mencoder -v -o /dev/null -endpos 0 -nosound -ovc frameno $1 | grep " fps\| frames"`
FPS=`echo "$TMP" | grep -o "[.0-9]\+ fps" | sed 's/.0\+ fps//'`
FRM=`echo "$TMP" | grep -o "for [.0-9]\+ frames" | sed 's/for \(.\+\) frames/\1/'`
bn=`basename $1 .avi`
W=640
H=480
RATE=1
suf=jpg # jpg much slower for next processing too
#ffmpeg -v quiet -r $((FPS*RATE)) -i $1 -an -ss 10 -r 1 -y -f image2 -s ${W}x$H "fr${bn}_%04d.$suf" &
#sleep 10
HSQ=6 # half size of on-screen square
CMP="compare -quiet -metric AE -fuzz 5% -compose Src"
debug () {
	[ "$DEBUG" ] && echo "$1" "$2" 1>&2
	return 0
}
check_rgb () {
	for RGB in $1 $2 $3
	do
		[ $RGB -lt 219 -o $RGB -gt 254 ] && debug -n " out of range: $RGB" && return 1 # lower limit changed from 233 for 12,2 series
	done
	DIF=`echo $1 $2 $3 | tr ' ' '\n' | sort -n -r | tr '\n' '-' | sed 's/-[^-]*-/-/' | sed 's/-$/\n/' | bc`
	[ $DIF -gt 22 ] && debug -n " out of gray: $DIF" && return 1 # limit change from 16 for 2 series - blue colored
	return 0
}
check_point () {
	local F=$1
	local X=$2
	local Y=$3
	debug "try: $X $Y"
	set -- `identify -format "%[fx:p{$X,$Y}.r*255] %[fx:p{$X,$Y}.g*255] %[fx:p{$X,$Y}.b*255]" $F` # returns RGB
	check_rgb $1 $2 $3
}
check_slide_point () {
	local F=$1
	local X=$2
	local Y=$3
	local FMT=""
	for XY in "$X,$Y" "$X,$((Y-HSQ))" "$((X-HSQ)),$Y" "$HSQ,$Y" "$HSQ,$((Y-HSQ))" "$((HSQ*2)),$Y"
	do
		FMT="$FMT %[fx:p{$XY}.r*255] %[fx:p{$XY}.g*255] %[fx:p{$XY}.b*255]"
	done
	SKIPS=6
	set -- `identify -format "$FMT" $F` # returns RGB
	while [ $# -gt 1 ]
	do
		R=$1
		G=$2
		B=$3
		shift; shift; shift
		check_rgb $R $G $B
		if [ $? -eq 0 ]; then
			for((c=0;c<$SKIPS;c++)); do shift; done
			debug -n " ok"
			SKIPS=6
		else
			[ $SKIPS -eq 0 ] && debug -e " $(tput rev)declined$(tput sgr0)" && return 1
			SKIPS=$((SKIPS-3))
		fi
	done 
	debug ""
	return 0
}
find_slide_bound () {
	local F=$1
	local X=$2
	Y=$3 # it's return
	local UP=0
	while [ $((Y-UP)) -ge $HSQ ]
	do
		ORD=$((UP+(Y-UP)/2)) # binary search
		debug -n "check $UP-$Y=$ORD"
		check_slide_point $F $X $ORD
		[ $? -eq 1 ] && UP=$ORD || Y=$ORD
	done
}
#mkdir notslide notslide2 nearlywhite dup
# many files not exist yet
for i in `ls fr${bn}_* | head`
#for I in `seq -w 1 9999`
do
#	i=fr${bn}_$I.$suf
	[ ! -f $i ] && sleep 1 && [ ! -f $i ] && break
	debug -n "$i"
	#check_slide_point $i $((W-10)) $((H-5))
	#[ $? -ne 0 ] && rm $i && continue
	#find_slide_bound $i $((W-10)) $((H-5-2*HSQ))
	#[ $((H-Y)) -le 35 ] && rm $i && continue # too small height for letters
	#[ $Y -gt $HSQ ] && mogrify -fill white -draw "rectangle 0,0 $W,$Y" $i
	#mogrify -white-threshold 78% $i
	[ -z "$PREV" ] && convert -fill white -draw "rectangle 0,0 $W,$H" $i white.png
	CDIF=`$CMP $i white.png cwhite.png 2>&1 | cut -d" " -f1 | cut -d. -f1`
	[ $CDIF -le 10 ] && rm $i && continue # too small pixels to carry info
	if [ "$PREV" ]; then
		CPDIF=`$CMP cwhite.png pwhite.png null: 2>&1 | cut -d" " -f1 | cut -d. -f1`
		debug -n "dif: $PDIF $CPDIF $CDIF"
		[ $CPDIF -lt $CDIF -a `perl -e "print ($PDIF < $CDIF || abs($PDIF/$CDIF-1) < 0.01 ? 1 : 0)"` -eq 1 ] && debug -n " $(tput rev)removed$(tput sgr0)" && rm $PREV
		exit
	fi
	debug ""
	mv cwhite.png pwhite.png
	PDIF=$CDIF
	PREV=$i
done
T1=`date +%s`
wait
T2=`date +%s`
T2=$((T2-T1))
[ $T2 -gt 1 ] && echo "WARN: ffmpeg lag: $T2"
rm -f pwhite.png white.png
exit
P="p{630,475}" # X was 635, before 6 series, and Y is 470 in some of 16 series
RED=0.85098
GREEN=0.866667
BLUE=0.909804
# 12 series
RED=0.92549
GREEN=0.913725
BLUE=0.941176
# 6-11,13-16 series
RED=0.968627
GREEN=0.976471
BLUE=0.972549
for i in fr${bn}_*
do
	identify -format "%[fx:(abs($P.r-$RED)+abs($P.g-$GREEN)+abs($P.b-$BLUE))<0.08]" $i | grep -q -x 0 && rm $i && continue
	[ "$PREV" ] && F=`compare -metric MSE $PREV $i null: 2>&1 | cut -d" " -f1` && [ `echo "$F < 20" | bc` -eq 1 ] && rm $PREV
	PREV=$i
done
#for ss in `seq 1 $((FPS*RATE)) $FRM`
#do
#	ffmpeg -loglevel quiet -i $1 -an -ss $ss -an -r $FPS -vframes 2 -y -f image2 -s 640x480 $bn-snapshot_$ss.jpg
#done
